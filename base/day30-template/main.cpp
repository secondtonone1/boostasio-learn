#include <iostream>
#include "template.h"
#include <vector>


int main() {
//    std::cout << maxValue(1,2) << std::endl;
//    std::cout << maxValue(1.1,2.2) << std::endl;
//    std::cout << maxValue('a','b') << std::endl;
//    std::cout << maxValue("abc","def") << std::endl;

    Pair<int, int> p1(1,2);
    p1.print();

    Pair<double, std::string> p2(1.1,"abc");
    p2.print();
    std::vector<int> v1;

    FixedArray<int, 5> arr;
    for(int i = 0; i < 5; ++i) {
        arr[i] = i * 10;
    }

    arr.print();

    ContainerPrinter<std::vector, int> vecPrinter;
    std::vector<int> vec = {1,2,3,4,5};
    vecPrinter.print(vec);

    ContainerPrinter<std::list, int> listPrinter;
    std::list<int> list = {10,20,30,40,50};
    listPrinter.print(list);

    Printer<int> intPrinter;
    intPrinter.print(10);

    Printer<std::string> strPrinter;
    strPrinter.print("Hello");

    double value = 10.5;
    Pair<std::string, double* > pair_special2("abc",&value);
    pair_special2.print();

    printValue("hello ");
    int abc = 10;
    printValue(&abc);

    printAll(10,20.5,"abc");
    coutAll(10,20.5,"abc");

    std::cout << sumAll(10,20.5,23.4,7,3.1415926) << std::endl;

    std::cout << allNot(false, false, true) << std::endl;
    std::cout << allNot(false, false, false) << std::endl;

    std::cout << sumLeftFold(1,2,3,4,5) << std::endl;
    std::cout << multiRightFold(1,2,3,4,5) << std::endl;
    printFoldAll(1,2,3,4,5);
    MyPoint myp1{1,2}, myp2{3,4}, myp3{5,6};
    auto result = sumPoints(myp1, myp2, myp3);
    std::cout << result.x << " " << result.y << std::endl;
    return 0;
}
