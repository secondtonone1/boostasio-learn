#include <iostream>
#include "Singleton.h"
#include <thread>
#include <mutex>
#include <thread>
int main() {
    //auto inst = Single2Hungry::GetInst();
    system("chcp 65001 > nul");
//    std::cout << "s1 addr is " << &Single2::GetInst() << std::endl;
//    std::cout << "s2 addr is " << &Single2::GetInst() << std::endl;

    std::mutex mtx;
    std::thread t1([&](){
        SingleNet::GetInst();
        std::lock_guard<std::mutex> lock(mtx);
        std::cout << "s1 addr is " << SingleNet::GetInst() << std::endl;
    });

    std::thread t2([&](){
        SingleNet::GetInst();
        std::lock_guard<std::mutex> lock(mtx);
        std::cout << "s2 addr is " << SingleNet::GetInst() << std::endl;
    });

    t1.join();
    t2.join();

    return 0;
}
