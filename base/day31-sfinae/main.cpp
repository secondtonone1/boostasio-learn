#include <iostream>
#include "sfinae.h"
#include <string>
#include <vector>


int main() {
    int x = 10;
    print_type(x);
    double y = 3.14;
    print_type(3.14);
    const char* cstr = "Hello Zach!";
    print_type(cstr);
    std::string str = "Hello Zack!";
    print_type(str);
    print_type(&x);

    cout_type(x);

    cout_type(3.14);

    cout_type(cstr);

    cout_type(str);
    cout_type(&x);

    WithFoo wf;
    call_foo(wf);

    TypePrinter<WithFoo>::print();

    TypePrinter<WithValueType>::print();

    return 0;
}
