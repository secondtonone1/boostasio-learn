#include <iostream>
#include "Logger.h"

int main() {
    int a = 10;
    Logger<int>::log(a);
    int * ptr = &a;
    Logger<int*>::log(ptr);
    std::string str = "Hello zack!";
    Logger<std::string>::log(str);
    std::cout<< "\nLogging multiple parameters: " << std::endl;
    double* pNull = nullptr;
    logAll(a, ptr, str, pNull);

    std::cout << "5!= " << Factorial<5>::value << std::endl;
    std::cout << "0!= " << Factorial<0>::value << std::endl;

    std::cout << "&5!= " << &Factorial<5>::value << std::endl;
    std::cout << "&0!= " << &Factorial<0>::value << std::endl;

    std::cout << "Fibonacci<10> = " << Fibonacci<10>::value << std::endl;
    std::cout << "Fibonacci<20> = " << Fibonacci<20>::value << std::endl;

    static_assert(is_addable<int>::value, "int should be addable");
    static_assert(!is_addable<void*>::value, "void* should be addable");

    int result = Sum<1,2,3,4,5>::value;
    std::cout << "Sum<1,2,3,4,5> = " << result << std::endl;

    using list = TypeList<int, double,char>;
    using third_type = TypeAt<list,2>::type;
    std::cout << "TypeAt<list,2> = " << typeid(third_type).name() << std::endl;
    return 0;
}
