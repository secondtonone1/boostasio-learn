cmake_minimum_required(VERSION 3.28)
project(day32_templatemeta)

set(CMAKE_CXX_STANDARD 17)

add_executable(day32_templatemeta main.cpp
        Logger.h
        Logger.cpp)
