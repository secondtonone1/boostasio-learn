//
// Created by secon on 2024/9/8.
//
#include "example.h"
#include <iostream>

MyClass::MyClass() {
    // 构造函数实现
}

void MyClass::myFunction() {
    std::cout << "Hello from MyClass::myFunction!" << std::endl;
}