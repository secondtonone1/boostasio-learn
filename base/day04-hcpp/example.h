//
// Created by secon on 2024/9/8.
//

#ifndef DAY04_HCPP_EXAMPLE_H
#define DAY04_HCPP_EXAMPLE_H
class MyClass {
public:
    MyClass(); // 构造函数声明
    void myFunction(); // 成员函数声明
};
#endif //DAY04_HCPP_EXAMPLE_H
